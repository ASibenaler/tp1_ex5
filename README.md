## Exercice 5

# Objectif
L'objectif de cet exercice est de modifier la classe SimpleComplexCalculator pour que ses méthodes renvoient un message d'erreur lorsqu'une variable d'entrée n'est ni un entier ni un réel.

# Réalisation
On modifie tout d'abord la classe SimpleComplexCalculator pour que les méthodes prennent directement les complexes en entrée, afin de faciliter les tests.
On ajoute ensuite des condition au début de chaque méthode, vérifiant que lec complexes sont des listes de deux entiers ou flotants. Dans le cas de la division, on avait déjà ajouté une condition empêchant de diviser pas 0.

