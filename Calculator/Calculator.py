#!/usr/bin/env python
# coding : utf-8

"""
author : Arnaud Sibenaler
"""


class SimpleComplexCalculator:
    """classe SimpleComplexCalculator"""

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def fsum(self):
        """somme des attributs complexes"""
        if (isinstance(self.a, list)) & (isinstance(self.b, list)) & \
        (len(self.a) == len(self.b) == 2) & \
        ((type(self.a[0]) in [int,float] )) & ((type(self.a[1]) in [int,float] )) & \
        ((type(self.b[0]) in [int,float] )) & ((type(self.b[1]) in [int,float] )):
            return [
                self.a[0] + self.b[0],
                self.a[1] + self.b[1],
            ]
        return "ERROR"

    def substract(self):
        """difference des attributs complexes"""
        if (isinstance(self.a, list)) & (isinstance(self.b, list)) & \
        (len(self.a) == len(self.b) == 2) & \
        ((type(self.a[0]) in [int,float] )) & ((type(self.a[1]) in [int,float] )) & \
        ((type(self.b[0]) in [int,float] )) & ((type(self.b[1]) in [int,float] )):
            return [
                self.a[0] - self.b[0],
                self.a[1] - self.b[1],
            ]
        return "ERROR"

    def multiply(self):
        """produit des attributs complexes"""
        if (isinstance(self.a, list)) & (isinstance(self.b, list)) & \
        (len(self.a) == len(self.b) == 2) & \
        ((type(self.a[0]) in [int,float] )) & ((type(self.a[1]) in [int,float] )) & \
        ((type(self.b[0]) in [int,float] )) & ((type(self.b[1]) in [int,float] )):
            return [
                self.a[0] * self.b[0]
                - self.a[1] * self.b[1],
                self.a[0] * self.b[1]
                + self.a[1] * self.b[0],
            ]
        return "ERROR"

    def divide(self):
        """quotient des attributs complexes"""
        if not ((isinstance(self.a, list)) or (isinstance(self.b, list)) or \
        (len(self.a) == len(self.b) == 2) & \
        ((type(self.a[0]) in [int,float] )) or ((type(self.a[1]) in [int,float] )) or \
        ((type(self.b[0]) in [int,float] )) or ((type(self.b[1]) in [int,float] ))):
            return "ERROR"
        if (self.b[0] ** 2 + self.b[1] ** 2) != 0:
            return [
                (
                    self.a[0] * self.b[0]
                    + self.a[1] * self.b[1]
                )
                / (self.b[0] ** 2 + self.b[1] ** 2),
                (
                    self.a[1] * self.b[0]
                    + self.a[0] * self.b[1]
                )
                / (self.b[0] ** 2 + self.b[1] ** 2),
            ]
        return "Division par 0 !"
