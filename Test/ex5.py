#!/usr/bin/env python
# coding : utf-8

"""
author : Arnaud Sibenaler
"""


from Calculator.Calculator import SimpleComplexCalculator


if __name__ == "__main__":
    TEST = SimpleComplexCalculator([1.1, 2.2], [3.3, 4.4])
    print("Somme : ", TEST.fsum())
    print("Soustraction : ", TEST.substract())
    print("Produit : ", TEST.multiply())
    print("Division : ", TEST.divide())
